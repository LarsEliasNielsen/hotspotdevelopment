<?php

/******************************************************************************
* Let's say that $response contains the response from a database-query, and   *
* that $posts is each response.                                               *
******************************************************************************/

$response = array();
$posts = array();

$posts[] = array(
  'title' => 'Økonom: Nu stiger dine fleksrenter',
  'url' => 'http://finans.tv2.dk/nyheder/article.php/id-70699330:%C3%B8konom-nu-stiger-dine-fleksrenter.html'
);
$posts[] = array(
  'title' => 'Skolestart: Her er 5 ting du ikke bør sige til dit barn',
  'url' => 'http://go.tv2.dk/sexogsamliv/2013-08-12-skolestart-her-er-5-ting-du-ikke-b%C3%B8r-sige-til-dit-barn'
);
$posts[] = array(
  'title' => 'Fængsel for at sprøjte på Thorning',
  'url' => 'http://nyhederne.tv2.dk/article.php/id-70701162:f%C3%A6ngsel-for-at-spr%C3%B8jte-p%C3%A5-thorning.html'
);
$posts[] = array(
  'title' => 'Apple må ikke fotografere i Norge',
  'url' => 'http://beep.tv2.dk/nyheder/apple-m%C3%A5-ikke-fotografere-i-norge'
);

$response['posts'] = $posts;

$fp = fopen('results.json', 'w');
fwrite($fp, json_encode($response));
fclose($fp);

?>

<!-- Output JSON result -->
<p>JSON result:</p>
<iframe src="results.json" width="800" height="300"></iframe>